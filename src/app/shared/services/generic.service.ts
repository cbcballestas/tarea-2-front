import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GenericService<T> {
  constructor(
    protected http: HttpClient,
    @Inject(String) protected url: string
  ) {}

  listar(): Observable<T[]> {
    return this.http.get<T[]>(this.url);
  }

  listarPorId(id: number): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  guardar(obj: T) {
    return this.http.post(this.url, obj);
  }

  modificar(obj: T) {
    return this.http.put(this.url, obj);
  }

  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
