import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Persona } from '../models/persona';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PersonaService extends GenericService<Persona> {
  private personaCambio = new Subject<Persona[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(http, `${environment.HOST}/personas`);
  }

  setPersonaCambio(personas: Persona[]) {
    this.personaCambio.next(personas);
  }

  getPersonaCambio() {
    return this.personaCambio.asObservable();
  }

  setMensajeCambio(value: string) {
    this.mensajeCambio.next(value);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
}
