import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Venta } from '../models/venta';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VentaService {
  private mensajeCambio = new Subject<string>();
  URL = `${environment.HOST}/ventas`;

  constructor(private http: HttpClient) {}

  setMensajeCambio(value: string) {
    this.mensajeCambio.next(value);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  guardar(venta: Venta) {
    return this.http.post(this.URL, venta);
  }
}
