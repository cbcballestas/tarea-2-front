import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Producto } from '../models/producto';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductoService extends GenericService<Producto> {
  private productoCambio = new Subject<Producto[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(http, `${environment.HOST}/productos`);
  }

  setProductoCambio(productos: Producto[]) {
    this.productoCambio.next(productos);
  }

  getProductoCambio() {
    return this.productoCambio.asObservable();
  }

  setMensajeCambio(value: string) {
    this.mensajeCambio.next(value);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
}
