import { Component, OnInit } from '@angular/core';
import { VentaService } from '../../../../shared/services/venta.service';
import { Persona } from '../../../../shared/models/persona';
import { PersonaService } from '../../../../shared/services/persona.service';
import { Observable } from 'rxjs';
import { ProductoService } from '../../../../shared/services/producto.service';
import { Producto } from 'src/app/shared/models/producto';
import { DetalleVenta } from '../../../../shared/models/detalleVenta';
import { Venta } from '../../../../shared/models/venta';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-venta-nuevo',
  templateUrl: './venta-nuevo.component.html',
  styleUrls: ['./venta-nuevo.component.css'],
})
export class VentaNuevoComponent implements OnInit {
  personas$: Observable<Persona[]>;
  productos$: Observable<Producto[]>;

  importe: number;
  idPersonaSeleccionada: number;
  idProductoSeleccionado: number;
  cantidad: number;

  maxFecha: Date = new Date();
  fechaSeleccionada: Date = new Date();

  detalleVenta: DetalleVenta[] = [];

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private productoService: ProductoService,
    private ventaService: VentaService,
    private personaService: PersonaService
  ) {}

  ngOnInit(): void {
    this.listarPersonas$();
    this.listarProductos$();
  }

  operar(): void {
    const persona = new Persona();
    persona.idPersona = this.idPersonaSeleccionada;

    const venta = new Venta();
    venta.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    venta.importe = this.importe;
    venta.persona = persona;
    venta.detalleVenta = this.detalleVenta;

    this.ventaService.guardar(venta).subscribe(() => {
      this.snackBar.open('VENTA GUARDADA', 'AVISO', { duration: 2000 });
      this.router.navigateByUrl('/persona');
    });
  }

  estadoBotonRegistrar(): boolean {
    return (
      this.detalleVenta.length === 0 ||
      this.idPersonaSeleccionada === 0 ||
      this.importe === null
    );
  }

  agregarProducto(): void {
    const detalle = new DetalleVenta();
    this.productoService
      .listarPorId(this.idProductoSeleccionado)
      .subscribe((data) => {
        detalle.cantidad = this.cantidad;
        detalle.producto = data;
        this.detalleVenta.push(detalle);
        this.cantidad = null;
        this.idProductoSeleccionado = null;
      });
  }

  borrarProducto(indice: number): void {
    this.detalleVenta.splice(indice, 1);
  }

  private listarPersonas$(): void {
    this.personas$ = this.personaService.listar();
  }

  private listarProductos$(): void {
    this.productos$ = this.productoService.listar();
  }
}
