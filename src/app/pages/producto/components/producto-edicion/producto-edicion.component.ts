import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';

import { ProductoService } from '../../../../shared/services/producto.service';
import { Producto } from '../../../../shared/models/producto';

@Component({
  selector: 'app-producto-edicion',
  templateUrl: './producto-edicion.component.html',
  styleUrls: ['./producto-edicion.component.css'],
})
export class ProductoEdicionComponent implements OnInit {
  form: FormGroup;
  edicion = false;

  constructor(
    private fb: FormBuilder,
    private productoService: ProductoService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.edicion = id != null;

      if (this.edicion) {
        this.productoService.listarPorId(id).subscribe((data) => {
          this.setEditData(data);
        });
      }
    });
  }

  private buildForm(): void {
    this.form = this.fb.group({
      id: [0],
      nombre: ['', Validators.required],
      marca: ['', Validators.required],
    });
  }

  private setEditData(producto: Producto): void {
    this.form.setValue({
      id: producto.idProducto,
      nombre: producto.nombre,
      marca: producto.marca,
    });
  }

  operar(): void {
    if (this.form.valid) {
      const producto = new Producto();
      producto.idProducto = this.form.value['id'];
      producto.nombre = this.form.value['nombre'];
      producto.marca = this.form.value['marca'];

      if (this.edicion) {
        this.productoService
          .modificar(producto)
          .pipe(
            switchMap(() => {
              return this.productoService.listar();
            })
          )
          .subscribe((data) => {
            this.productoService.setProductoCambio(data);
            this.productoService.setMensajeCambio('PRODUCTO ACTUALIZADO');
          });
      } else {
        this.productoService
          .guardar(producto)
          .pipe(
            switchMap(() => {
              return this.productoService.listar();
            })
          )
          .subscribe((data) => {
            this.productoService.setProductoCambio(data);
            this.productoService.setMensajeCambio('PRODUCTO GUARDADO');
          });
      }

      this.router.navigateByUrl('/producto');
    } else {
      this.form.markAllAsTouched();
    }
  }
}
