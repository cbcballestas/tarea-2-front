import { Component, OnInit, ViewChild } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Producto } from '../../shared/models/producto';
import { ProductoService } from '../../shared/services/producto.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css'],
})
export class ProductoComponent implements OnInit {
  dataSource: MatTableDataSource<Producto>;
  displayedColumns = ['idProducto', 'nombre', 'marca', 'acciones'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private productoService: ProductoService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.productoService.getProductoCambio().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.productoService.getMensajeCambio().subscribe((data) => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.productoService.listar().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  filtrar(value: string): void {
    this.dataSource.filter = value.trim().toLowerCase();
  }
  eliminar(producto: Producto): void {
    this.productoService
      .eliminar(producto.idProducto)
      .pipe(
        switchMap(() => {
          return this.productoService.listar();
        })
      )
      .subscribe((data) => {
        this.productoService.setProductoCambio(data);
        this.productoService.setMensajeCambio('PRODUCTO ELIMINADO');
      });
  }
}
