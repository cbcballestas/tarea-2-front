import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs/operators';

import { Persona } from '../../../../shared/models/persona';
import { PersonaService } from '../../../../shared/services/persona.service';

@Component({
  selector: 'app-persona-edicion',
  templateUrl: './persona-edicion.component.html',
  styleUrls: ['./persona-edicion.component.css'],
})
export class PersonaEdicionComponent implements OnInit {
  persona: Persona;
  titulo: string;

  constructor(
    private personaService: PersonaService,
    private dialogRef: MatDialogRef<PersonaEdicionComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Persona
  ) {}

  ngOnInit(): void {
    this.persona = new Persona();
    this.persona.idPersona = this.data.idPersona;
    this.persona.nombres = this.data.nombres;
    this.persona.apellidos = this.data.apellidos;
  }

  guardar(): void {
    if (this.persona != null && this.persona.idPersona > 0) {
      // Modificar
      this.personaService
        .modificar(this.persona)
        .pipe(
          switchMap(() => {
            return this.personaService.listar();
          })
        )
        .subscribe((data) => {
          this.personaService.setPersonaCambio(data);
          this.personaService.setMensajeCambio('REGISTRO MODIFICADO');
        });
    } else {
      // Guardar
      this.personaService
        .guardar(this.persona)
        .pipe(
          switchMap(() => {
            return this.personaService.listar();
          })
        )
        .subscribe((data) => {
          this.personaService.setPersonaCambio(data);
          this.personaService.setMensajeCambio('REGISTRO GUARDADO');
        });
    }
    this.cancelar();
  }

  cancelar(): void {
    this.dialogRef.close();
  }
}
