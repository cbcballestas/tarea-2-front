import { Component, OnInit, ViewChild } from '@angular/core';
import { switchMap } from 'rxjs/operators';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { PersonaEdicionComponent } from './components/persona-edicion/persona-edicion.component';
import { PersonaService } from '../../shared/services/persona.service';
import { Persona } from 'src/app/shared/models/persona';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css'],
})
export class PersonaComponent implements OnInit {
  dataSource: MatTableDataSource<Persona>;
  displayedColumns = ['idPersona', 'nombres', 'apellidos', 'acciones'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private personaService: PersonaService,
    private dialogRef: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.personaService.getPersonaCambio().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    this.personaService.getMensajeCambio().subscribe((data) => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.personaService.listar().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  filtrar(value: string): void {
    this.dataSource.filter = value.trim().toLowerCase();
  }

  eliminar(persona: Persona): void {
    this.personaService
      .eliminar(persona.idPersona)
      .pipe(
        switchMap(() => {
          return this.personaService.listar();
        })
      )
      .subscribe((data) => {
        this.personaService.setPersonaCambio(data);
        this.personaService.setMensajeCambio('SE ELIMINÓ');
      });
  }

  abrirDialogo(persona?: Persona): void {
    this.dialogRef.open(PersonaEdicionComponent, {
      width: '250px',
      data: persona != null ? persona : new Persona(),
    });
  }
}
